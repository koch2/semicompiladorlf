from typing import List


class funcoes:
    # inicia classe
    def __init__(self):
        self.texto=''
        self.variavel=0
        self.conteudo=[]
        self.arquivoLeitura='lista.txt'
        self.arquivoEscrita = 'saida_lexico.txt'

#leitura do arquivo  inicial
    def LerArquivo(self):
        arq = open(self.arquivoLeitura, 'r')
        self.texto = arq.readlines()
        arq.close()
#leitura da linha
    def LerLinha(self):
       #print(self.texto);
       if self.variavel<len(self.texto):
           a=self.texto[self.variavel]
           self.variavel+=1
           return a



    #listade tokens <rotulo, string>

    ## tok1 - DelimEsq - (

    # tok2 - DelimDir - )

    # tok3 - OpUn - not

    # tok4 - OpBin
    # tok401 - +
    # tok402 - -
    # tok403 - *
    # tok404 - /
    # tok405 - gt
    # tok406 - lt
    # tok407 - geq
    # tok408 - leq
    # tok409 - eq
    # tok410 - neq
    # tok411 - and
    # tok412 - or

    # tok5 - TestCond - if

    # tok6 - NomeFunc - defun

    # tok7 - Num (<digito> <num>)

    # tok8- Id(<letra><seqsimb>) // seq simb ´pode ser <digito seqsimb> ou <letra seqsimb>

    def ehDelimitadorEsquerda(self, caracter):
        delimitadores = "("
        if caracter in delimitadores:
            return True
        return False

    def ehDelimitadorDireita(self, caracter):
        delimitadores = ")"
        if caracter in delimitadores:
            return True
        return False


    def qualTokenDelimitador(self, entrada):
        # String com os operadores componentes da linguagem
        delimitadores = "()"
        posicao = delimitadores.find(entrada)
        if(posicao==0):
            return "tok1 DelimEsq " + str(delimitadores[posicao])+""
        elif(posicao==1):
            return "tok2 DelimDir " + str(delimitadores[posicao])+""

    def ehLetra(self, caracter):
        letra = 'abcdefghijklmnopqrstuvxwyz'
        if caracter in letra:
            return True
        return False

    def ehNumero(self, caracter):
        numero = '0123456789'
        if caracter in numero:
            return True
        return False

   # def qualTocketId(self):


    def ehOperador(self, entrada):
        operadores = '+ - * / gt lt geq leq eq neq and or'.split()
        if entrada in operadores:
            return True
        return False

    def qualTokenOperador(self, entrada):
        operadores = '+ - * / gt lt geq leq eq neq and or'.split()
        posicao = 0

        for x in operadores:
            if x == entrada:
                break
            posicao += 1
        if(posicao<9):
            return "tok40"+str(posicao+1)+" OpBin " + str(operadores[posicao])
        else:
            return "tok4"+str(posicao+1)+" OpBin " + str(operadores[posicao])

    def ehOpedadorUnario(self, entrada):
        operadore = 'not'.split()
        if entrada in operadore:
            return True
        return False

    def ehReservada(self, entrada):
        reservadas = "defun if gt lt geq leq eq neq not and or".split()
        if entrada in reservadas:
            return True
        return False

    def analisa(self):
        # Abre o arquivo de saida do programa
        arquivo_saida = open(self.arquivoEscrita, 'w')
        # Verifica se o arquivo de entrada existe no diretorio em questao


        # Abre o arquivo de entrada do programa
        a=funcoes()
        a.LerArquivo()

        # Le a primeira linha
        linha_programa = a.LerLinha()

        # Variavel que indica a linha do caracter_atual
        numero_linha = 1

        # Percorre o programa linha por linha
        while linha_programa:
            i = 0
            tamanho_linha = len(linha_programa)
            while i < tamanho_linha:  # Percorre os caracteres da linha
                caracter_atual = linha_programa[i]
                caractere_seguinte = None
                # Soh posso pegar o caractere_seguinte se ele existe na linha
                if ((i + 1) < tamanho_linha):
                    caractere_seguinte = linha_programa[i + 1]
                    # ===================================================================================
                # Verifica se o caracter eh um delimitador - OK
                if (self.ehDelimitadorEsquerda(caracter_atual) or self.ehDelimitadorDireita(caracter_atual)):
                    arquivo_saida.write(str(self.qualTokenDelimitador(caracter_atual))+' '+ str(numero_linha)+' '+str(i)+ '\n')
                # ===================================================================================
                # Consumindo comentarios de linha - OK
                elif (caracter_atual == '/' and caractere_seguinte == '/'):
                    # Fazendo o programa pular para a proxima linha
                    i = tamanho_linha
                # ===================================================================================
                # Consumindo comentarios de bloco - OK
                elif (caracter_atual == '/' and caractere_seguinte == '*'):
                    cont = True  # Variavel que impedirah o loop a seguir de continuar caso
                    # seja falsa, isso acontece com erro fim inesperado de arquivo
                    linha_comeco = numero_linha  # Guardo a linha que o bloco comecou, para caso
                    # o erro de bloco nao fechado ocorrer o programa
                    # poderah indicar o comeco do erro
                    while cont and not (caracter_atual == '*' and caractere_seguinte == '/'):
                        # Soh posso pegar o caractere atual e o proximo se ele existe na linha
                        if ((i + 2) < tamanho_linha):
                            i += 1
                            caracter_atual = linha_programa[i]
                            caractere_seguinte = linha_programa[i + 1]
                        else:
                            linha_programa = arquivo.readline()  # Le a proxima linha
                            tamanho_linha = len(linha_programa)
                            numero_linha += 1
                            i = -1
                            if (not linha_programa):
                                arquivo_saida.write("Erro Lexico - Comentario de bloco nao fechado - linha: %d\n" % linha_comeco)
                                cont = False
                    i += 1  # Faco isso para que nao considere o '/' do final do bloco (na composicao */) no proximo loop
                # ===================================================================================
                # Verificando se o elemento eh um operador
                elif self.ehOperador(caracter_atual):
                    arquivo_saida.write(str(self.qualTokenOperador(caracter_atual)) +' '+ str(numero_linha)+' '+str(i)+'\n')

                # ===================================================================================
                # Verificando se o elemento em questao eh um numero - OK
                elif (self.ehNumero(caracter_atual)):
                    string_temp = caracter_atual
                    i += 1
                    j = 0  # Vai contar se o numero tem pelo menos 1 digito depois do '.'
                    caracter_atual = linha_programa[i]
                    while (self.ehNumero(caracter_atual) and (i + 1 < tamanho_linha)):
                        string_temp += caracter_atual
                        i += 1
                        caracter_atual = linha_programa[i]
                    else:
                        if(self.ehLetra(caracter_atual)):
                            arquivo_saida.write( "Erro Lexico - numero com letra não pode - linha: %d\n" % linha_comeco)
                        else:
                            arquivo_saida.write('tok7 Num ' + string_temp +' '+ str(numero_linha)+' '+str(i)+'\n')
                            if (not self.ehNumero(caracter_atual)):
                                i -= 1
                # ===================================================================================
                # Verificando identificadores ou palavras reservadas - OK
                elif (self.ehLetra(caracter_atual)):
                    # Apos verificar que o primeiro caractere da palavra era uma letra, vou percorrendo o identificador
                    # ateh encontrar um caractere que nao possa ser de identificadores ou ateh o final da linha
                    string_temp = caracter_atual
                    i += 1
                    algum_erro = False
                    while i < tamanho_linha:
                        caractere_seguinte = None
                        caracter_atual = linha_programa[i]
                        if (i + 1 < tamanho_linha):
                            caractere_seguinte = linha_programa[i + 1]
                        if (self.ehLetra(caracter_atual) or self.ehNumero(caracter_atual)):
                            string_temp += caracter_atual
                        elif (self.ehDelimitadorEsquerda(caracter_atual) or self.ehDelimitadorDireita(caracter_atual) or caracter_atual == ' ' or caracter_atual == '\t' or caracter_atual == '\r'):
                            i -= 1
                            break
                        elif caracter_atual != '\n':
                            arquivo_saida.write(
                                "Erro Lexico - Identificador com caracter invalido: " + caracter_atual + " - linha: %d\n" % numero_linha)
                            algum_erro = True
                            break
                        i += 1  # Passando o arquivo ateh chegar ao final do identificador/palavra reservada

                    if (algum_erro):
                        while (i + 1 < tamanho_linha):
                            i += 1
                            caracter_atual = linha_programa[i]
                            if self.ehDelimitador(caracter_atual) or caracter_atual == ' ' or caracter_atual == '\t' or caracter_atual == '\r' or caracter_atual == '/':
                                i -= 1  # Preciso voltar um elemento da linha para que o delimitador seja reconhecido no momento certo
                                break
                    else:  # Se nao houver erros basta verificar se o elemento eh palavra reservada tambem
                        if (self.ehReservada(string_temp)):
                            if self.ehOperador(string_temp):
                                arquivo_saida.write(self.qualTokenOperador(string_temp) +' '+ str(numero_linha)+' '+str(i)+ '\n')
                            else:
                                if(string_temp=="if"):
                                    arquivo_saida.write("tok5 TestCond "  + string_temp +' '+ str(numero_linha)+' '+str(i)+'\n')
                                elif(string_temp=="defun"):
                                    arquivo_saida.write("tok6 NomeFunc " + string_temp +' '+ str(numero_linha)+' '+str(i)+'\n')
                                elif (string_temp == "not"):
                                    arquivo_saida.write("tok3 OpUn " + string_temp +' '+ str(numero_linha)+' '+str(i)+ '\n')

                        else:
                            arquivo_saida.write('tok8 Id ' + string_temp +' '+ str(numero_linha)+' '+str(i)+ '\n')

                # ===================================================================================
                # Verificando Erros Lexicos - Caracter Invalido
                # Note que os caracteres especiais \n, \t, \r e espaco sao desconsiderados como caracteres invalidos
                # por aparecerem constantemente no codigo em questao
                elif caracter_atual != '\n' and caracter_atual != ' ' and caracter_atual != '\t' and caracter_atual != '\r':
                    arquivo_saida.write(
                        'Erro Lexico - Caracter Invalido: ' + caracter_atual + ' - linha: %d\n' % numero_linha)
                # ===================================================================================
                i += 1  # Incrementando a leitura dos caracteres da linha lida no momento

            linha_programa =a.LerLinha()  # Le a proxima linha
            numero_linha += 1
        # Fim do programa
        arquivo_saida.write('$')
        # Fim do arquivo de saida
        arquivo_saida.close
        # ========================== FIM DO ANALISADOR LEXICO