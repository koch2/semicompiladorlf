

from Node import Node









# Declarando Classe do Analisador Sintatico
class AnalisadorSintatico():
    # ========================== DECLARACAO DE METODOS DA CLASSE
    # Metodo construtor da classe
    def __init__(self):

        self.arquivo_entrada = "saida_lexico.txt"
        self.arquivo_saida = "saida_sintatico.txt"

        self.tem_erro_sintatico = False

        self.arquivo_saida = open(self.arquivo_saida, 'w')
        # Verifica se o arquivo de entrada existe no diretorio em questao

        # Abre o arquivo de entrada do programa
        self.arquivo = open(self.arquivo_entrada, 'r')
        self.tokens = self.arquivo.readlines()
        self.arquivo.close()
        self.i = 0
        self.j = 0
        self.linha_atual = ""

        # Definindo tabela de simbolos analise semantica
        self.registro_tab = {}
        self.constantes_tab = {}
        self.variaveis_globais_tab = {}
        self.funcoes_tab = {}
        self.algoritmo_tab = {}
        self.node=0

    # Faz o cabecote de leitura apontar para o proximo token da lista
    def next_token(self):
        self.i += 1
        self.linha_atual = self.tokens[self.i][self.tokens[self.i].find('->') + 2: -1]

    def conteudo_token(self):
        return self.tokens[self.i][: self.tokens[self.i].find('->')]





    # <programa> := <lista-de-funçoes>
    def programa(self):
        if ("Erro Lexico" in self.tokens[self.i]):
            self.i += 1
        left = Node()
        left.value.append("<lista-de-funçoes>")
        subNo =Node()
        left.no.append(subNo)
        self.node=Node()
        self.node.value.append("<programa>")
        self.node.no.append(left)
        self.lista_funcoes(subNo)

        if (self.tem_erro_sintatico):
            print("Verifique os erros sintaticos e tente compilar novamente")
            self.arquivo_saida.write("Verifique os erros sintaticos e tente compilar novamente\n")
        else:
            if ("$" in self.tokens[self.i]):
                print("Cadeia de tokens na analise sintatica reconhecida com sucesso")
                self.arquivo_saida.write("Cadeia de tokens reconhecida com sucesso\n")
            else:
                print("Fim de Programa Nao Encontrado!")
                self.arquivo_saida.write("Fim de Programa Nao Encontrado!")
        # Fechando arquivo de saida
        self.arquivo_saida.close()


    # <lista-de-funçoes> := <funcao> | <funcao> <lista-de-funcoes>
    def lista_funcoes(self,no):
        if ("Erro Lexico" in self.tokens[self.i]):
            self.i += 1

        no.value.append("<funcao>")
        left = Node()
        no.no.append(left)
        self.funcao(left)  # chama identificador de funcao

        if ("$" not in  self.tokens[self.i]):
            print(self.conteudo_token());
            no.value.append("<lista-de-funcoes>")
            left = Node()
            no.no.append(left)
            self.lista_funcoes(left)  # chama recursiva pra ver se tem mais func
        # tratar erros aqui depois?


    # <funcao> := (defun <id> <(params)> <corpo>)
    def funcao(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        if 'tok1 DelimEsq ' in self.tokens[self.i]:  # lendo parenteses (
            self.next_token()
            no.value.append("(")
            no.no.append(0)
            if 'tok6 NomeFunc' in self.tokens[self.i]:  # lendo defun
                self.next_token()
                no.value.append("defun")
                no.no.append(0)
                if 'tok8 Id' in self.tokens[self.i]:  # lendo id
                    id_registro = self.conteudo_token()
                    no.value.append("<id>(" + id_registro+")")
                    left = Node()
                    no.no.append(left)
                    self.next_token()
                    if 'tok1 DelimEsq' in self.tokens[self.i]:
                        no.value.append("(")
                        no.no.append(0)
                        self.next_token()
                        no.value.append("<params>")
                        left = Node()
                        no.no.append(left)
                        self.parametros(left)
                        if 'tok2 DelimDir' in self.tokens[self.i]:
                            no.value.append(")")
                            no.no.append(0)
                            self.next_token()
                            no.value.append("<corpo>")
                            left = Node()
                            no.no.append(left)
                            self.corpo(left)
                            if 'tok2 DelimDir' in self.tokens[self.i]:
                                self.next_token()
                                no.value.append(")")
                                no.no.append(0)

                                # fim correto - verificar se parenteses foi fechado


    # <params> := <id> | <id> <params>
    def parametros(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        if 'tok8 Id' in self.tokens[self.i]:
            id_registro = self.conteudo_token()
            no.value.append("<id>("+id_registro+")")
            no.no.append(0)
            self.next_token()
            if 'tok8 Id' in self.tokens[self.i]:
                no.value.append("<params>")
                left = Node()
                no.no.append(left)
                self.parametros(left)
                # tratar erros de id


    # <corpo> := (<corpo'>|<corpo''>) | <exp>
    def corpo(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        if 'tok1 DelimEsq' in self.tokens[self.i]:
            self.next_token()
            no.value.append("(")
            no.no.append(0)
            if 'tok5 TesteCond' in self.tokens[self.i]:
                self.next_token()
                no.value.append("<corpol>")
                left = Node()
                no.no.append(left)
                self.corpo1(left)
                if 'tok2 DelimDir' in self.tokens[self.i]:
                    no.value.append(")")
                    no.no.append(0)
                    self.next_token()
            elif 'tok8 Id' in self.tokens[self.i]:
                self.next_token()
                no.value.append("<corpoll>")
                left = Node()
                no.no.append(left)
                self.corpo11(left)
                if 'tok2 DelimDir' in self.tokens[self.i]:
                    self.next_token()
                    no.value.append(")")
                    no.no.append(0)
            else:
                no.value.append("<exp>")
                left = Node()
                no.no.append(left)
                self.expressao(left)
                if 'tok2 DelimDir' in self.tokens[self.i]:
                    self.next_token()
                    no.value.append(")")
                    no.no.append(0)

        elif 'tok7 Num' in self.tokens[self.i] or 'tok8 Id' in self.tokens[self.i]:
            no.value.append("<exp>")
            left = Node()
            no.no.append(left)
            self.expressao(left)


    # <corpo'> := if <cond> <corpo> <corpo>
    def corpol(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        self.next_token()
        no.value.append("if")
        no.no.append(0)
        no.value.append("<cond>")
        left = Node()
        no.no.append(left)
        self.cond(left)
        no.value.append("<condo>")
        left = Node()
        no.no.append(left)
        self.corpo(left)
        no.value.append("<condo>")
        left = Node()
        no.no.append(left)
        self.corpo(left)


    # <corpo''> := <id> <corpo>
    def corpo11(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        no.value.append("<id>("+self.conteudo_token()+")")
        no.no.append(0)
        self.next_token()
        no.value.append("<corpo>")
        left = Node()
        no.no.append(left)
        self.corpo(left)


    # <exp> := <num> | <id> | (exp')
    def expressao(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        if 'tok7 Num' in self.tokens[self.i]:
            no.value.append("<num>("+self.conteudo_token()+")")
            no.no.append(0)
            self.next_token()
        elif 'tok8 Id' in self.tokens[self.i]:
            no.value.append("<id>(" + self.conteudo_token() + ")")
            no.no.append(0)
            self.next_token()
        elif 'tok1 DelimEsq' in self.tokens[self.i]:
            self.next_token()
            no.value.append("(")
            no.no.append(0)
            no.value.append("<exp'>")
            left = Node()
            no.no.append(left)
            self.exp1(left)
            if 'tok2 DelimDir' in self.tokens[self.i]:
                self.next_token()
                no.value.append(")")
                no.no.append(0)
                # saiu
        else:  # é tipo lido no corpo
            self.expr1(no)


    # <exp'> := + <exp> <exp> | - <exp> <exp> | * <exp> <exp> | / <exp> <exp>
    def expr1(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        if ('tok401 OpBin' in self.tokens[self.i]
                or 'tok402 OpBin' in self.tokens[self.i]
                or 'tok403 OpBin' in self.tokens[self.i]
                or 'tok404 OpBin' in self.tokens[self.i]):
            no.value.append(self.conteudo_token())
            no.no.append(0)
            self.next_token()
            no.value.append("<exp'>")
            left = Node()
            no.no.append(left)
            self.expressao(left)
            no.value.append("<exp'>")
            left = Node()
            no.no.append(left)
            self.expressao(left)
       # else:


    # erro que não vai ser tratado

    # <cond> := ( gt <exp> <exp> | lt <exp> <exp> | geq <exp> <exp> | leq <exp> <exp> | eq <exp> <exp> |
    # neq <exp> <exp> | 3 not <cond>  | and <cond> <cond> | or <cond> <cond>)
    def cond(self,no):
        if "Erro Lexico" in self.tokens[self.i]:
            self.i += 1
        if 'tok1 DelimEsq' in self.tokens[self.i]:
            no.value.append(self.conteudo_token())
            no.no.append(0)
            self.next_token()
            if ('tok405 OpBin ' in self.tokens[self.i]
                    or 'tok406 OpBin ' in self.tokens[self.i]
                    or 'tok407 OpBin ' in self.tokens[self.i]
                    or 'tok408 OpBin ' in self.tokens[self.i]
                    or 'tok409 OpBin ' in self.tokens[self.i]
                    or 'tok410 OpBin ' in self.tokens[self.i]):
                no.value.append(self.conteudo_token())
                no.no.append(0)
                self.next_token()
                no.value.append("<exp'>")
                left = Node()
                no.no.append(left)
                self.expressao()
                no.value.append("<exp'>")
                left = Node()
                no.no.append(left)
                self.expressao()
                if 'tok2 DelimDir' in self.tokens[self.i]:
                    self.next_token()
                    # saiu

            if ('tok411 OpBin ' in self.tokens[self.i]
                    or 'tok412 OpBin ' in self.tokens[self.i]):
                no.value.append(self.conteudo_token())
                no.no.append(0)
                self.next_token()
                no.value.append("<cond>")
                left = Node()
                no.no.append(left)
                self.cond()
                no.value.append("<cond>")
                left = Node()
                no.no.append(left)
                self.cond()
                if 'tok2 DelimDir' in self.tokens[self.i]:
                    self.next_token()
                    # saiu
            if 'tok3 OpUn' in self.tokens[self.i]:
                no.value.append(self.conteudo_token())
                no.no.append(0)
                self.next_token()
                no.value.append("<cond>")
                left = Node()
                no.no.append(left)
                self.cond(left)
                if 'tok2 DelimDir' in self.tokens[self.i]:
                    no.value.append(self.conteudo_token())
                    no.no.append(0)
                    self.next_token()
                    # saiu






