# SemiCompiladorLF

Matheus Koch  e Jiovana Gomes

Deve colocar no arquivo  "lista.txt" o  codigo  do programa para ele gerar o tok sao esses: 

    #listade tokens <rotulo, rotulo, string>
    ## tok1 - DelimEsq - (
    # tok2 - DelimDir - )
    # tok3 - OpUn - not
    # tok401  - OpBin- +
    # tok402 - OpBin- -
    # tok403 - OpBin- *
    # tok404 - OpBin- /
    # tok405 - OpBin- gt
    # tok406 - OpBin- lt
    # tok407 - OpBin- geq
    # tok408 - OpBin- leq
    # tok409 - OpBin- eq
    # tok410 - OpBin- neq
    # tok411 - OpBin- and
    # tok412 - OpBin- or
    # tok5 - TestCond - if
    # tok6 - NomeFunc - defun
    # tok7 - Num (valor)
    # tok8- Id(string) 


Foi  usado  esse compilador como  exemplo,  para entender os conteitos.  A parte de tocket foi baseado  de acordo  com  ele.  "https://github.com/eng-assys/CompiladorPhyton"